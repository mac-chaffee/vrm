echo 'Installing golang...'
sudo yum update
sudo yum install -y golang

echo 'Downloading the server...'
wget https://gitlab.com/mac-chaffee/voice-resource-monitor/raw/master/server/server.go

echo 'Compiling the server...'
go build server.go

echo 'Done!'
echo 'Run the server with "./server"'
echo 'By default, it runs on port 8080 but you can change that'
echo 'by setting the VOICE_MONITOR_PORT environment variable and restarting the server'
