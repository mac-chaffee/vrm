package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"syscall"
)

func handler(output http.ResponseWriter, req *http.Request) {
	ram := getRAM()
	cpu := getCPU()

	output.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(output, `{"ram": "%s", "cpu": "%s"}`, ram, cpu)
}

func getRAM() string {
	info := &syscall.Sysinfo_t{}
	err := syscall.Sysinfo(info)
	if err != nil {
		panic("Could not get system info")
	}
	usedRam := (info.Totalram - info.Freeram) / 1000000
	return fmt.Sprintf("%d megabytes", usedRam)
}

func getCPU() string {
	// Parse /proc/stat
	stat_bytes, err := ioutil.ReadFile("/proc/stat")
	if err != nil {
		panic(err)
	}

	stats := string(stat_bytes)
	firstLine := strings.Split(stats, "\n")[0]
	vals := strings.Split(firstLine, " ")[2:]
	parsedVals := make([]float64, len(vals))
	totalTime := 0.0
	// Convert everything to float64 and sum
	for i := 0; i < len(vals); i++ {
		parsedVal, err := strconv.ParseFloat(vals[i], 64)
		if err != nil {
			panic(err)
		}
		parsedVals[i] = parsedVal
		totalTime += parsedVal
	}
	timeSpentIdle := parsedVals[3] / totalTime
	timeSpentNotIdle := 1.0 - timeSpentIdle
	cpuPercent := timeSpentNotIdle * 100.0

	return fmt.Sprintf("%.1f percent", cpuPercent)
}

func main() {
	// Register the request handler
	http.HandleFunc("/", handler)
	// Set the port from the env var, or default to 8080
	port, err := strconv.Atoi(os.Getenv("VOICE_MONITOR_PORT"))
	if err != nil {
		port = 8080
	}
	portString := fmt.Sprintf(":%d", port)
	fmt.Printf("Serving on port %d\n", port)
	// Start the server
	http.ListenAndServe(portString, nil)
}
