from string import Template
import urllib
import json
import boto3


# --------------- Helper that builds all of the responses ----------------------
def build_response(title, output, reprompt_text):
    return {
        'version': '1.0',
        'sessionAttributes': {},
        'response': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': output
            },
            'card': {
                'type': 'Simple',
                'title': title,
                'content': output
            },
            'reprompt': {
                'outputSpeech': {
                    'type': 'PlainText',
                    'text': reprompt_text
                }
            },
            'shouldEndSession': True
        }
    }


# --------------- Helper Functions used in Intents ------------------
def get_ip_and_port(given_slots):
    """Parse the slots and build a url"""
    expected_slots = ['one', 'two', 'three', 'four', 'port']
    parsed_vals = {}
    # Make sure all the values are present and valid
    for slot_name in expected_slots:
        if 'value' not in given_slots[slot_name] or given_slots[slot_name]['value'] == '?':
            return None, None
        parsed_vals[slot_name] = given_slots[slot_name]['value']
    template = Template("$one.$two.$three.$four")

    return template.substitute(parsed_vals), parsed_vals['port']


def check_existing_servers(session):
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('servers')
    response = table.get_item(
        Key={
            'userId': session['user']['userId'],
        }
    )
    if 'Item' not in response:
        return []
    return response['Item']['servers']


def list_to_string(lst):
    final_string = ''
    if len(lst) == 2:
        final_string += lst[0]
        final_string += " and "
        final_string += lst[1]
    elif len(lst) == 1:
        final_string += lst[0]
    elif not lst:
        final_string = ''
    else:
        for i in range(0, len(lst) - 1):
            final_string += lst[i]
            final_string += ", "
        final_string += "and "
        final_string += lst[len(lst) - 1]
    return final_string


# --------------- Intents ------------------
def add_server_to_db(intent, session):
    """Intent that saves the server to DynamoDB"""
    card_title = intent['name']
    ip, port = get_ip_and_port(intent['slots'])
    existing_servers = check_existing_servers(session)
    if ip and port:
        url = f'http://{ip}:{port}'
        if url in existing_servers:
            speech_output = f"I am already monitoring {ip} on port {port}"
            reprompt_text = None
        else:
            dynamodb = boto3.resource('dynamodb')
            table = dynamodb.Table('servers')
            table.put_item(
                Item={
                    'userId': session['user']['userId'],
                    'servers': [*existing_servers, url]
                }
            )
            speech_output = f"I am now monitoring {ip} on port {port}"
            reprompt_text = None
    else:
        speech_output = ("I'm not sure I understood your ip and port. "
                         "Please try again.")
        reprompt_text = ("Please tell me the ip and port number. For example,"
                         "ask V R M to monitor 10.0.0.256 on port 8080")
    return build_response(card_title, speech_output, reprompt_text)


def get_resource_usage(intent, session):
    """Intent that gets the resource usage from all servers stored for the user"""
    # Get the server url from DynamoDB
    servers = check_existing_servers(session)
    if not servers:
        speech_output = "I am not monitoring any servers for you"
        return build_response(intent['name'], speech_output, None)
    speech_output = ''
    for server in servers:
        # Connect to the go server
        usage_data = urllib.request.urlopen(f"{server}").read()
        usage_data = json.loads(usage_data)
        # Create the response
        host = server.replace("http://", "").split(':')[0]
        speech_output += f"The CPU usage on {host} is at {usage_data['cpu']} "
        speech_output += f"and it is using {usage_data['ram']} of ram. "

    return build_response(intent['name'], speech_output, None)


def get_servers(intent, session):
    """Intent to list all servers currently being watched"""
    servers = check_existing_servers(session)
    if not servers:
        speech_output = "I am not monitoring any servers for you"
        return build_response(intent['name'], speech_output, None)
    server_strs = []
    for server in servers:
        host, port = server.replace('http://', '').split(':')
        server_strs.append(f"{host} on port {port}")
    speech_output = f"I am monitoring {list_to_string(server_strs)}"
    return build_response(intent['name'], speech_output, None)


def delete_servers(intent, session):
    """Intent to delete all servers currently being watched"""
    servers = check_existing_servers(session)
    if not servers:
        speech_output = "I am not monitoring any servers for you"
        return build_response(intent['name'], speech_output, None)

    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('servers')
    table.delete_item(
        Key={
            'userId': session['user']['userId'],
        }
    )
    if len(servers) > 1:
        speech_output = f"I've stopped monitoring {len(servers)} servers"
    else:
        speech_output = f"I've stopped monitoring {len(servers)} server"
    return build_response(intent['name'], speech_output, None)


# --------------- Events ------------------
def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """
    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']

    # Dispatch to your skill's intent handlers
    if intent_name == "GetResourceUse":
        return get_resource_usage(intent, session)
    elif intent_name == "AddServer":
        return add_server_to_db(intent, session)
    elif intent_name == "GetServers":
        return get_servers(intent, session)
    elif intent_name == "DeleteServers":
        return delete_servers(intent, session)
    else:
        raise ValueError("Invalid intent %s" % intent_name)


# --------------- Main handler ------------------
def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    print(event)
    expected_id = "amzn1.ask.skill.151b726d-1e6d-45a0-85b1-76a5e88c7115"
    if event['session']['application']['applicationId'] != expected_id:
        raise ValueError("Invalid Application ID")

    if event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        pass  # Because this skill doesn't use sessions
    else:
        raise ValueError("Unrecognized request %s" % event['request']['type'])
