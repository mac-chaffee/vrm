# Voice Resource Monitor

Use Alexa to monitor the CPU and RAM usage of your
servers. A HackNC 2017 project.

## Demo

https://www.youtube.com/watch?v=QC5cHgdKbpo

## Infrastructure Diagram

![infrastructure](/infrastructure.jpg)

## Installation
```
wget -O - https://gitlab.com/mac-chaffee/voice-resource-monitor/raw/master/server/install.sh | bash
```
